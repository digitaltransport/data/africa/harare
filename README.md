# Harare

On 18 June 2018, The World Bank contracted WhereIsMyTransport to supply public transportation data for The World Bank’s Land Use and Transport Accessibility Tool project. The data has been provided in GTFS format, a free and open template for entering data related to basic public transport systems. The data provided in this report includes the following files:

* trips.txt
* stop_times.txt
* stops.txt
* shapes.txt
* routes.txt
* frequencies.txt
* feed_info.txt
* fare_rules.txt
* fare_attributes.txt
* calendar.txt
* agency.txt

The Harare dataset was collected between 2 July 2018 to 14 July 2018. The collection in Harare was managed by Graeme Leighton. We worked with local data collectors who used the WhereIsMyTransport proprietary mobile app to collect the following information:

* Route shape
* Route headway
* Agency name
* Route name
* Stop location
* Stop name
* Route frequency – for peak and off-peak times

This data was collected for the kombi network and the city shuttle (fixed route taxi) network in Harare. While we indicated in our technical proposal that we would be collecting the Zupco bus network, this network now only runs intercity service between major cities in Zimbabwe.The dataset was processed in our offices between 14 July to 31 July 2018 by Khathutshelo Maumela and William Morris. The processing of the Harare data was managed by Chienne Wolmarans. The dataset contains a total of 496 routes and 2484 stops.
The link to the data can be found here. The password to access the folder is GTFS.

### Edited Route Type

`route_type` property under `routes.txt` has been update to enable accessibility analysis with Conveyal. The updated GTFS files have `Edited` suffix.
